# Fennel Syntax

Visual Studio Code extension

Fennel language syntax highlighting - grammar only

## Status

Experimental. Work in progress.

Build out of curiosity and dissatisfaction with existing Fennel grammars.

Problems with existing grammars:
- Multi-symbols are treated as single tokens
- Shorthand-strings are not highlighted (with default color schemes)
- Lua builtins are highlighted even though they are just normal functions (from Fennels point of view)
- Bugs: Highlights in the middle of names:
  - (fn **table**-index-of [...])
  - (analyze-**fn** def.definition)

## Features

- Highlights Fennel builtin special forms and macros differently, depending on where they are used:
  - valid: (**local** foo 42)
  - invalid: (foo **local** 42)
- Highlights symbols differently when used as first item of a list:
  - (**foo** a b 42)
- Highlights only the function name part of multi-symbols:
  - (foo.bar:**baz** 42)
  - (foo.**bar** a b c)
- Highlights **:shorthand-strings** and table.**field**.**access** in the same color.
- Does *not* highlight Lua builtin functions. These are just normal functions.

I am abusing the `keyword.control.` scope to get prominent highlighting for
shorthand strings and table field access.

## Development

Start the "Run Extension" task in vscode (F5)

### Installation

```
npm install --ignore-scripts
npx vsce package
codium --install-extension foo.vsix
```

## License

MIT License
